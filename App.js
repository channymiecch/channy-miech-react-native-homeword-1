/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component, Fragment} from 'react';
import {Platform, StyleSheet, Text, View, Image,TextInput, CheckBox, Button} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component{  
state = { check : ''}
handleChange = (e) => {
  this.setState({
    check: e.target.value
  });
}

  render() {
    return (
      <Fragment>
        <View style={styles.container}>
            <Image source={{uri: 'https://s.w.org/style/images/about/WordPress-logotype-wmark.png'}}
            style={styles.imageStyle} />
        </View>
        <View style={styles.containerText}>
            <Text style={styles.textStyle}>Username or Email Address</Text>
            <TextInput style={styles.textInputStyle} autoCapitalize={'words'}/>
            <Text style={styles.textStyle}>Password</Text>
            <TextInput secureTextEntry={true}  style={styles.textInputStyle} autoCapitalize={'none'}/>
        </View>
        <View style={styles.containerCheckbox}>
          <View style={{ flexDirection: 'row', flex: 3}}>
            <CheckBox  checkedIcon='dot-circle-o' uncheckedIcon='circle-o' 
              checked={this.state.checked}
              onChange={this.handleCheckboxChange}
            />
            <Text style={{marginTop: 5}}>Remember Me</Text>
          </View>
          <View style={styles.containerButton}>
            <Button style={styles.buttonLoginStyle} title="Login"  />
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  containerText: {
    backgroundColor: '#F5FCFF',
    paddingLeft: 20,
    paddingRight: 20
  },
  containerCheckbox: {
    flexDirection: 'row',
    marginTop: 25,
    flex: 7,
    paddingLeft: 20,
    paddingRight: 20
  },
  imageStyle: {
    width: 200, 
    height: 200, 
    marginTop: 5,
  },
  textStyle: {
    fontSize: 20,
    marginTop: 20,
    textAlign: 'left'
  },
  textInputStyle: {
    borderWidth: 1,
    borderColor: 'gray',
    height: 50,
    width: 370,
    marginTop: 20,
  },
  buttonLoginStyle: {
    textAlign: 'right',
  },
  containerButton: {
    flexDirection: 'column',
    flex: 1,
    
  }
});
